from environment import data_collector_host, rabbitmq_host
import requests
import json
from diffTimeDistanceOfBus import SpeedAverage
import pika
from datetime import datetime


class UpdateCapabilities:
    json = None
    speed_average = 0
    date_time = None

    def get_json(self):
        location_url = data_collector_host+"/resources/data"
        now = datetime.now()
        self.date_time = now.strftime('%Y-%m-%d')
        data = {"sensor_value":{"capabilities":"location"},
                "start_range":self.date_time+"T00:00:00",
                "end_range":self.date_time+"T23:59:59"}
        headers = {"Content-Type": "application/json"}
        response = requests.post(location_url, json=data, headers=headers)
        self.json = response.content.decode("utf-8")

    def process(self):
        self.data = json.loads(self.json)
        processed_data = []

        resources = self.data['resources']
        for resource in resources:
            uuid = resource['uuid']
            locations = resource['capabilities']['location']
            for location in locations:
               position = json.loads(location['value'])
               timestamp = location['date']
               processed_data.append([uuid,position,timestamp])

        self.speed_average = SpeedAverage.process(processed_data)

    def publish_speed_average(self):
        connection = pika.BlockingConnection(pika.ConnectionParameters(
                                             rabbitmq_host))

        channel = connection.channel()
        channel.exchange_declare(exchange='data_stream', type='topic')

        for uuid, value in self.speed_average.items():
            result = json.dumps({"value": value, "timestamp": self.date_time})
            channel.basic_publish(exchange='data_stream',
                                  routing_key=uuid+".speed_average",
                                  body=result)
        connection.close()

a = UpdateCapabilities()
a.get_json()
a.process()
a.publish_speed_average()
